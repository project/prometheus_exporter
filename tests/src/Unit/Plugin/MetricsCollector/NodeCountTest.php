<?php

declare(strict_types=1);

namespace Drupal\Tests\prometheus_exporter\Unit\Plugin\MetricsCollector;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\prometheus_exporter\Plugin\MetricsCollector\NodeCount;
use Drupal\Tests\UnitTestCase;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @coversDefaultClass \Drupal\prometheus_exporter\Plugin\MetricsCollector\NodeCount
 * @group prometheus_exporter
 */
class NodeCountTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * @covers ::collectMetrics
   */
  public function testCollectMetrics() {

    $nodeQuery1 = $this->prophesize(QueryInterface::class);
    $nodeQuery1->accessCheck(TRUE)->willReturn($nodeQuery1);
    $nodeQuery1->count()->willReturn($nodeQuery1);
    $nodeQuery1->execute()->willReturn(12);

    $nodeQuery2 = $this->prophesize(QueryInterface::class);
    $nodeQuery2->accessCheck(TRUE)->willReturn($nodeQuery2);
    $nodeQuery2->condition('type', 'article')->willReturn($nodeQuery2);
    $nodeQuery2->count()->willReturn($nodeQuery2);
    $nodeQuery2->execute()->willReturn(42);

    $nodeTypeStorage = $this->prophesize(EntityStorageInterface::class);
    $nodeTypeStorage->getQuery()->willReturn($nodeQuery1, $nodeQuery2);

    $definition = [
      'provider' => 'node_count',
      'description' => 'Test description',
    ];

    $collector = new NodeCount([], 'node_count', $definition, $nodeTypeStorage->reveal());
    $collector->setConfiguration([
      'enabled' => TRUE,
      'weight' => 0,
      'settings' => [
        'bundles' => [
          'article' => 'article',
        ],
      ],
    ]);

    $metrics = $collector->collectMetrics();

    $this->assertCount(1, $metrics);
    /** @var \PNX\Prometheus\Metric $metric */
    $metric = $metrics[0];
    $this->assertEquals('gauge', $metric->getType());
    $this->assertEquals('drupal_node_count_total', $metric->getName());
    $this->assertEquals('Test description', $metric->getHelp());

    $labelledValues = $metric->getLabelledValues();
    $this->assertCount(2, $labelledValues);
    $value1 = $labelledValues[0];
    $this->assertEquals(12, $value1->getValue());
    $value2 = $labelledValues[1];
    $this->assertEquals(42, $value2->getValue());
    $this->assertEquals(['bundle' => 'article'], $value2->getLabels());
  }

}
