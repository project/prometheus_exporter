<?php

declare(strict_types=1);

namespace Drupal\Tests\prometheus_exporter\Unit\Plugin\MetricsCollector;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\prometheus_exporter\Plugin\MetricsCollector\UserCount;
use Drupal\Tests\UnitTestCase;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @coversDefaultClass \Drupal\prometheus_exporter\Plugin\MetricsCollector\UserCount
 * @group prometheus_exporter
 */
class UserCountTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * @covers ::collectMetrics
   */
  public function testCollectMetrics() {
    $total = 12;
    $blocked = 5;
    $activated = $total - $blocked;
    $userQuery1 = $this->prophesize(QueryInterface::class);
    $userQuery1->accessCheck(TRUE)->willReturn($userQuery1);
    $userQuery1->count()->willReturn($userQuery1);
    $userQuery1->execute()->willReturn($total);

    $userQuery2 = $this->prophesize(QueryInterface::class);
    $userQuery2->accessCheck(TRUE)->willReturn($userQuery2);
    $userQuery2->condition('status', 1)->willReturn($userQuery2);
    $userQuery2->count()->willReturn($userQuery2);
    $userQuery2->execute()->willReturn($activated);

    $userQuery3 = $this->prophesize(QueryInterface::class);
    $userQuery3->accessCheck(TRUE)->willReturn($userQuery3);
    $userQuery3->condition('status', 0)->willReturn($userQuery3);
    $userQuery3->count()->willReturn($userQuery3);
    $userQuery3->execute()->willReturn($blocked);

    $userTypeStorage = $this->prophesize(EntityStorageInterface::class);
    $userTypeStorage->getQuery()
      ->willReturn($userQuery1, $userQuery2, $userQuery3);

    $configuration = [
      'enabled' => TRUE,
      'weight' => 0,
      'settings' => [],
    ];
    $definition = [
      'provider' => 'user_count',
      'description' => 'Test description',
    ];

    $collector = new UserCount($configuration, 'user_count', $definition,
      $userTypeStorage->reveal());

    $metrics = $collector->collectMetrics();

    // This collector returns a single Gauge.
    $this->assertCount(1, $metrics);

    /** @var \PNX\Prometheus\Metric $metric */
    $metric = $metrics[0];
    $this->assertEquals('gauge', $metric->getType());
    $this->assertEquals('drupal_user_count_total', $metric->getName());
    $this->assertEquals('Test description', $metric->getHelp());

    $labelledValues = $metric->getLabelledValues();
    // All, Activated, Blocked = 3.
    $this->assertCount(3, $labelledValues);
    $value1 = $labelledValues[0];
    $this->assertEquals($total, $value1->getValue());
    $this->assertEquals([], $value1->getLabels());

    $value2 = $labelledValues[1];
    $this->assertEquals($activated, $value2->getValue());
    $this->assertEquals(['status' => 'activated'], $value2->getLabels());

    $value3 = $labelledValues[2];
    $this->assertEquals($blocked, $value3->getValue());
    $this->assertEquals(['status' => 'blocked'], $value3->getLabels());
  }

}
