<?php

declare(strict_types=1);

namespace Drupal\Tests\prometheus_exporter\Unit\Plugin\MetricsCollector;

use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\prometheus_exporter\Plugin\MetricsCollector\QueueSizeCollector;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @coversDefaultClass \Drupal\prometheus_exporter\Plugin\MetricsCollector\QueueSizeCollector
 * @group prometheus_exporter
 */
class QueueSizeCollectorTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * @covers ::collectMetrics
   */
  public function testCollectMetrics() {

    $queue1 = $this->prophesize(QueueInterface::class);
    $queue1->numberOfItems()->willReturn(12);

    $queue2 = $this->prophesize(QueueInterface::class);
    $queue2->numberOfItems()->willReturn(42);

    $queueFactory = $this->prophesize(QueueFactory::class);
    $queueFactory->get(Argument::any())->willReturn($queue1, $queue2);

    $config = [
      'settings' => [
        'queues' => [
          'queue1' => 'queue1',
          'queue2' => 'queue2',
        ],
      ],
    ];
    $definition = [
      'provider' => 'test',
      'description' => 'Dummy description',
    ];
    $queuePluginManager = $this->prophesize(QueueWorkerManagerInterface::class);
    $queuePluginManager->getDefinitions()->willReturn([
      'queue1' => 'queue1',
      'queue2' => 'queue2',
    ]);
    $collector = new QueueSizeCollector($config, 'queue_size_test', $definition, $queueFactory->reveal(), $queuePluginManager->reveal());

    $metrics = $collector->collectMetrics();

    $this->assertCount(1, $metrics);

    /** @var \PNX\Prometheus\Metric $metric */
    $metric = $metrics[0];

    $this->assertEquals('gauge', $metric->getType());
    $this->assertEquals('drupal_queue_size_test_total', $metric->getName());
    $this->assertEquals('Dummy description', $metric->getHelp());

    $labelledValues = $metric->getLabelledValues();
    $this->assertCount(2, $labelledValues);

    $value1 = $labelledValues[0];
    $this->assertEquals(['queue' => 'queue1'], $value1->getLabels());
    $this->assertEquals(12, $value1->getValue());

    $value2 = $labelledValues[1];
    $this->assertEquals(['queue' => 'queue2'], $value2->getLabels());
    $this->assertEquals(42, $value2->getValue());

  }

}
