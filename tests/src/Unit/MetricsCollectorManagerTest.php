<?php

declare(strict_types=1);

namespace Drupal\Tests\prometheus_exporter\Unit;

use Drupal\prometheus_exporter\MetricsCollectorManager;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\prometheus_exporter\MetricsCollectorManager
 * @group prometheus_exporter
 */
class MetricsCollectorManagerTest extends UnitTestCase {

  /**
   * @covers ::syncPluginConfig
   */
  public function testSync() {

    $config = [
      'node_count' => [
        'id' => 'node_count',
        'provider' => 'prometheus_exporter',
        'enabled' => FALSE,
        'weight' => 0,
        'settings' => [
          'bundles' => [
            'article' => 'article',
          ],
        ],
      ],
      'mock_removed' => [
        'id' => 'mock_removed',
        'provider' => 'prometheus_exporter',
        'enabled' => FALSE,
        'weight' => 0,
        'settings' => [],
      ],
    ];

    $plugin_ids = ['mock_added', 'node_count'];

    $actual_config = MetricsCollectorManager::calculateSyncCollectorConfig($config, $plugin_ids);

    $expected_config = [
      'mock_added' => [
        'id' => 'mock_added',
        'provider' => 'prometheus_exporter',
        'enabled' => FALSE,
        'weight' => 0,
        'settings' => [],
      ],
      'node_count' => [
        'id' => 'node_count',
        'provider' => 'prometheus_exporter',
        'enabled' => FALSE,
        'weight' => 0,
        'settings' => [
          'bundles' => [
            'article' => 'article',
          ],
        ],
      ],
    ];

    // Assert that removed plugin config is gone and new plugins are added.
    $this->assertEquals($expected_config, $actual_config);

  }

}
