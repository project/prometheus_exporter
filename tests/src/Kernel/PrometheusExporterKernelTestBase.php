<?php

namespace Drupal\Tests\prometheus_exporter\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Abstract base class for Prometheus Exporter kernel tests.
 */
abstract class PrometheusExporterKernelTestBase extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'prometheus_exporter',
    'prometheus_exporter_update',
    'prometheus_exporter_test',
    'user',
    'node',
    'update',
    'system',
  ];

  /**
   * Kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['user']);
    $this->installSchema('system', 'sequences');
    // @see https://www.drupal.org/node/3143286
    if (version_compare(\Drupal::VERSION, '9.1', '<')) {
      $this->installSchema('system', 'key_value_expire');
    }
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');

    $this->httpKernel = $this->container->get('http_kernel');

    $config = $this->config('prometheus_exporter.settings');
    $settings = [];
    foreach ([
      'phpinfo',
      'node_count',
      'revision_count',
      'queue_size',
      'update_status',
      'test',
    ] as $item) {
      $settings[$item] = ['enabled' => TRUE];
    }
    $config->set('collectors', $settings);
    $config->save();

  }

}
