<?php

namespace Drupal\prometheus_exporter\Plugin\MetricsCollector;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\prometheus_exporter\Plugin\BaseMetricsCollector;
use PNX\Prometheus\Gauge;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Collects metrics for the total node count.
 *
 * @MetricsCollector(
 *   id = "node_count",
 *   title = @Translation("Node count"),
 *   description = @Translation("Total node count.")
 * )
 */
class NodeCount extends BaseMetricsCollector implements ContainerFactoryPluginInterface {

  /**
   * The node type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * UpdateStatusCollector constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $node_storage
   *   The node type storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $node_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->nodeStorage = $node_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('node')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $options = node_type_get_names();
    $form['bundles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Node Types'),
      '#description' => $this->t('Choose which node types you want to expose metrics for.'),
      '#default_value' => $this->getConfiguration()['settings']['bundles'] ?? [],
      '#options' => $options,
    ];
    if (empty($options)) {
      $form['bundles']['#markup'] = '<em>' . $this->t('No node types found') . '</em>';
    }
    return $form;
  }

  /**
   * Gets a count for this metric.
   *
   * @param string|null $bundle
   *   (optional) The bundle name to filter by.
   *
   * @return int
   *   The node count.
   */
  protected function getCount(?string $bundle = NULL) {
    $query = $this->nodeStorage->getQuery();
    $query->accessCheck(TRUE);
    if ($bundle) {
      $query->condition('type', $bundle);
    }
    return $query->count()->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function collectMetrics() {
    $gauge = new Gauge($this->getNamespace(), 'total', $this->getDescription());
    $gauge->set($this->getCount());
    $bundles = $this->getConfiguration()['settings']['bundles'] ?? [];
    array_map(function ($bundle) use ($gauge) {
      $gauge->set($this->getCount($bundle), ['bundle' => $bundle]);
    }, array_filter($bundles, function ($bundle) {
      // Disabled bundles have a value of 0.
      return $bundle !== 0;
    }));
    $metrics[] = $gauge;
    return $metrics;
  }

}
