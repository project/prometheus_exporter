<?php

namespace Drupal\prometheus_exporter\Plugin\MetricsCollector;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\prometheus_exporter\Plugin\BaseMetricsCollector;
use PNX\Prometheus\Gauge;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Collects metrics for the total node count.
 *
 * @MetricsCollector(
 *   id = "user_count",
 *   title = @Translation("User count"),
 *   description = @Translation("Total user count.")
 * )
 */
class UserCount extends BaseMetricsCollector implements ContainerFactoryPluginInterface {

  /**
   * The user type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * UserCount constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $user_storage
   *   The user type storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $user_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->userStorage = $user_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('user')
    );
  }

  /**
   * Gets a count for this metric.
   *
   * @param int|null $status
   *   (optional) The user status to filter by.
   *
   * @return int
   *   The user count.
   */
  protected function getCount(?int $status = NULL) {
    $query = $this->userStorage->getQuery();
    $query->accessCheck(TRUE);
    if (!is_null($status)) {
      $query->condition('status', $status);
    }
    $count = $query->count()->execute();
    return $count;
  }

  /**
   * {@inheritdoc}
   */
  public function collectMetrics() {
    $gauge = new Gauge($this->getNamespace(), 'total', $this->getDescription());
    $gauge->set($this->getCount());
    $gauge->set($this->getCount(TRUE), ['status' => 'activated']);
    $gauge->set($this->getCount(FALSE), ['status' => 'blocked']);
    $metrics[] = $gauge;
    return $metrics;
  }

}
