<?php

namespace Drupal\prometheus_exporter;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Collects metrics for export to prometheus.
 */
class MetricsCollectorManager {

  /**
   * The metrics collector plugin collection.
   *
   * @var \Drupal\prometheus_exporter\MetricsCollectorPluginCollection
   */
  protected $pluginCollection;

  /**
   * The settings, as defined in backend.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * MetricsCollectorCollector constructor.
   *
   * @param \Drupal\prometheus_exporter\MetricsCollectorPluginManager $pluginManager
   *   The plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configuration
   *   The configurationFactory.
   */
  public function __construct(MetricsCollectorPluginManager $pluginManager, ConfigFactoryInterface $configuration) {
    $this->configFactory = $configuration;
    $this->settings = $configuration->get('prometheus_exporter.settings');
    $collectorsSettings = $this->settings->get('collectors');
    $definitions = [];
    foreach ($pluginManager->getDefinitions() as $id => $definition) {
      $settings = $collectorsSettings[$id] ?? [];
      $definitions[$id] = array_merge($definition, $settings);
    }
    $this->pluginCollection = new MetricsCollectorPluginCollection($pluginManager, $definitions);
    $this->pluginCollection->sort();
  }

  /**
   * {@inheritdoc}
   */
  public function collectMetrics() {
    $metrics = [];
    /** @var \Drupal\prometheus_exporter\Plugin\MetricsCollectorInterface $collector */
    foreach ($this->pluginCollection->getIterator() as $collector) {
      if ($collector->isEnabled() && $collector->applies()) {
        $metrics = array_merge($metrics, $collector->collectMetrics());
      }
    }
    return $metrics;
  }

  /**
   * Syncs the plugin config removing any obsolete config.
   *
   * @see prometheus_exporter_modules_uninstalled()
   */
  public function syncPluginConfig() {
    $plugin_ids = $this->pluginCollection->getInstanceIds();
    $settings = $this->configFactory->getEditable('prometheus_exporter.settings');
    $config = $settings->get('collectors');
    // Check if any plugins have been installed or uninstalled.
    $updated_config = self::calculateSyncCollectorConfig($config, $plugin_ids);
    $settings->set('collectors', $updated_config);
    $settings->save();
  }

  /**
   * Calculates what the synchronized config should be.
   *
   * @param array $config
   *   An associative array of current collector config.
   * @param string[] $plugin_ids
   *   The plugin IDs of newly installed or uninstalled collector plugins.
   *
   * @return array
   *   The updated config.
   */
  public static function calculateSyncCollectorConfig(array $config, array $plugin_ids) {
    // Remove any config for removed plugins.
    $updated_config = array_intersect_key($config, array_flip($plugin_ids));
    $new_config_keys = array_diff_key(array_flip($plugin_ids), $config);
    foreach (array_keys($new_config_keys) as $key) {
      $updated_config[$key] = [
        'id' => $key,
        'provider' => 'prometheus_exporter',
        'enabled' => FALSE,
        'weight' => 0,
        'settings' => [],
      ];
    }
    return $updated_config;
  }

  /**
   * Gets the metrics collector plugins.
   *
   * @return \Drupal\prometheus_exporter\MetricsCollectorPluginCollection
   *   The plugins.
   */
  public function getPlugins() {
    return $this->pluginCollection;
  }

}
