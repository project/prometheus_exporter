# Prometheus Exporter

[![CircleCI](https://circleci.com/gh/previousnext/prometheus_exporter.svg?style=svg)](https://circleci.com/gh/previousnext/prometheus_exporter)


## Contents

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## Introduction

 * Exports Drupal metrics to be consumed by Prometheus scrapers.

 * WARNING: this module can expose sensitive information such as module versions
   which could be used to identify vulnerabilities. You should ensure access is
   only granted to trusted users via Basic Authentication or OAuth2, or protect
   with a web application firewall, or apache htaccess rules.

## Requirements

Requires the [previousnext/php-prometheus](https://packagist.org/packages/previousnext/php-prometheus) library. This will be required
automatically when installing by composer.


## Installation

 * Install with composer.

## Configuration

    1. Configure the Enabled collectors and Metrics Collector order for the
       module at 'Configuration > System > Prometheus Exporter'
       (/admin/config/system/prometheus_exporter).

## Maintainers

Current maintainers:
 * Kim Pepper (kim.pepper): https://www.drupal.org/u/kimpepper
