<?php

namespace Drupal\prometheus_exporter_comment\Plugin\MetricsCollector;

use Drupal\comment\CommentInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\prometheus_exporter\Plugin\BaseMetricsCollector;
use PNX\Prometheus\Gauge;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Collects metrics for module status.
 *
 * @MetricsCollector(
 *   id = "comment_count",
 *   title = @Translation("Comment Count"),
 *   description = @Translation("Provides metrics for comment counts.")
 * )
 */
class CommentCollector extends BaseMetricsCollector implements ContainerFactoryPluginInterface {

  /**
   * Any comment status.
   */
  const ANY_STATUS = -1;

  /**
   * The comment storage.
   *
   * @var \Drupal\comment\CommentStorageInterface
   */
  protected $commentStorage;

  /**
   * UpdateStatusCollector constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $comment_storage
   *   The comment storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $comment_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->commentStorage = $comment_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('comment')
    );
  }

  /**
   * Gets the comment count.
   *
   * @param int $status
   *   (optional) The comment status to filter by.
   *
   * @return int
   *   The comment count.
   */
  protected function getCount($status = self::ANY_STATUS) {
    $query = $this->commentStorage->getQuery();
    $query->accessCheck(TRUE);
    if ($status >= 0) {
      $query->condition('status', $status);
    }
    return $query->count()->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function collectMetrics() {
    $gauge = new Gauge($this->getNamespace(), 'total', $this->getDescription());
    $gauge->set($this->getCount());
    $gauge->set($this->getCount(CommentInterface::NOT_PUBLISHED), ['status' => 'not published']);
    $gauge->set($this->getCount(CommentInterface::PUBLISHED), ['status' => 'published']);
    $metrics[] = $gauge;
    return $metrics;
  }

}
