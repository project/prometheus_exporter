<?php

declare(strict_types=1);

namespace Drupal\Tests\prometheus_exporter_comment\Unit\Plugin\MetricsCollector;

use Drupal\comment\CommentInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\prometheus_exporter_comment\Plugin\MetricsCollector\CommentCollector;
use Drupal\Tests\UnitTestCase;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @coversDefaultClass \Drupal\prometheus_exporter_comment\Plugin\MetricsCollector\CommentCollector
 * @group prometheus_exporter
 */
class CommentCollectorTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * @covers ::collectMetrics
   */
  public function testCollect() {

    $commentQuery1 = $this->prophesize(QueryInterface::class);
    $commentQuery1->accessCheck(TRUE)->willReturn($commentQuery1);
    $commentQuery1->count()->willReturn($commentQuery1);
    $commentQuery1->execute()->willReturn(24);

    $commentQuery2 = $this->prophesize(QueryInterface::class);
    $commentQuery2->accessCheck(TRUE)->willReturn($commentQuery2);
    $commentQuery2->condition('status', CommentInterface::NOT_PUBLISHED)->willReturn($commentQuery2);
    $commentQuery2->count()->willReturn($commentQuery2);
    $commentQuery2->execute()->willReturn(1);

    $commentQuery3 = $this->prophesize(QueryInterface::class);
    $commentQuery3->accessCheck(TRUE)->willReturn($commentQuery3);
    $commentQuery3->condition('status', CommentInterface::PUBLISHED)->willReturn($commentQuery3);
    $commentQuery3->count()->willReturn($commentQuery3);
    $commentQuery3->execute()->willReturn(23);

    $comment_storage = $this->prophesize(EntityStorageInterface::class);
    $comment_storage->getQuery()->willReturn($commentQuery1, $commentQuery2, $commentQuery3);

    $definition = [
      'provider' => 'node_count',
      'description' => 'Test description',
    ];

    $collector = new CommentCollector([], 'comment_count', $definition, $comment_storage->reveal());

    $metrics = $collector->collectMetrics();

    $this->assertCount(1, $metrics);
    /** @var \PNX\Prometheus\Metric $metric */
    $metric = $metrics[0];
    $this->assertEquals('gauge', $metric->getType());
    $this->assertEquals('drupal_comment_count_total', $metric->getName());
    $this->assertEquals('Test description', $metric->getHelp());

    $labelledValues = $metric->getLabelledValues();
    $this->assertCount(3, $labelledValues);
    $value1 = $labelledValues[0];
    $this->assertEquals(24, $value1->getValue());

    $value2 = $labelledValues[1];
    $this->assertEquals(1, $value2->getValue());
    $this->assertEquals(['status' => 'not published'], $value2->getLabels());

    $value3 = $labelledValues[2];
    $this->assertEquals(23, $value3->getValue());
    $this->assertEquals(['status' => 'published'], $value3->getLabels());
  }

}
